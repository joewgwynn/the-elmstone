var gulp        = require('gulp');
var browserSync = require('browser-sync');
var sass        = require('gulp-sass');
var minifyCSS   = require('gulp-minify-css');
var plumber     = require('gulp-plumber');

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: './public'
    }
  });
});

gulp.task('sass', function() {
  return gulp.src('src/sass/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({ stream: true }))
  ;
});

// Default task to be run with `gulp`
gulp.task('default', ['sass', 'browser-sync'], function() {
  gulp.watch('src/sass/**/*.scss', ['sass']);
});

// Build task to be run with `gulp build`
gulp.task('build', ['sass', 'browser-sync']);